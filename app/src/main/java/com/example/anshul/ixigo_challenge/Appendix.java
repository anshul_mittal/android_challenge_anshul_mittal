package com.example.anshul.ixigo_challenge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by anshul on 4/1/17.
 */
public class Appendix {

    public void setAirlines(Map<String, String> airlines) {
        this.airlines = airlines;
    }

    public Map<String, String> getAirlines() {
        return this.airlines;
    }

    private Map<String, String> airlines;

    public Map<String, String> getAirports() {
        return this.airports;
    }

    public void setAirports(Map<String, String> airports) {
        this.airports = airports;
    }

    private Map<String, String> airports;

    public Map<String, String> getProviders() {
        return this.providers;
    }

    public void setProviders(Map<String, String> providers) {
        this.providers = providers;
    }

    private Map<String, String> providers;


}
