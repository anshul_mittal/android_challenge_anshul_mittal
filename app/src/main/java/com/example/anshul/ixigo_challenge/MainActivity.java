package com.example.anshul.ixigo_challenge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private static Retrofit retrofit;
    private static OkHttpClient client;
    private String Live_url = "https://firebasestorage.googleapis.com/";
    ListView listView;
    TextView fare, departure_time, arrival_time;
    List<Flight> flightList;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fare = (TextView) findViewById(R.id.fares);
        departure_time = (TextView) findViewById(R.id.departure);
        arrival_time = (TextView) findViewById(R.id.arrival);
        listView = (ListView) findViewById(R.id.listview);
        fare.setOnClickListener(this);
        departure_time.setOnClickListener(this);
        arrival_time.setOnClickListener(this);
        Retrofit retrofit = createRetrofitInstance(Live_url);
        // prepare call in Retrofit 2.0
        RetrofitInterface getResponse = retrofit.create(RetrofitInterface.class);
        Call<FlightResponse> call = getResponse.getData("https://firebasestorage.googleapis.com/v0/b/gcm-test-6ab64.appspot.com/o/ixigoandroidchallenge%2Fsample_flight_api_response.json?alt=media&token=d8005801-7878-4f57-a769-ac24133326d6");
        call.enqueue(new Callback<FlightResponse>() {
            @Override
            public void onResponse(Call<FlightResponse> call, Response<FlightResponse> response) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                FlightResponse responseModel = response.body();
                Appendix appendix = responseModel.getAppendix();
                flightList = new ArrayList<Flight>(responseModel.getFlights());
                adapter = new CustomAdapter(MainActivity.this, flightList, appendix);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<FlightResponse> call, Throwable t) {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            }
        });

    }


    public Retrofit createRetrofitInstance(String live_url) {
        client = new OkHttpClient
                .Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(live_url)
                .addConverterFactory(buildGsonConverter())
                .client(client)
                .build();
        return retrofit;
    }

    private static GsonConverterFactory buildGsonConverter() {
        //Gson parser for retrofit
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson myGson = gsonBuilder.create();
        return GsonConverterFactory.create(myGson);
    }

    @Override
    public void onClick(View v) {
        //sorting buttons
        switch (v.getId()) {
            case R.id.fares:
                Collections.sort(flightList);
                break;
            case R.id.departure:
                Collections.sort(flightList, new TakeoffComparator());
                break;
            case R.id.arrival:
                Collections.sort(flightList, new LandingTimeComparator());
                break;
        }
        adapter.notifyDataSetChanged();

    }
}

